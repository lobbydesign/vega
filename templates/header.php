<header class="site-navigation">
  <div class="site-navigation__container">
    <?php
    if (has_nav_menu('primary_navigation')) :
      wp_nav_menu([
        'theme_location' => 'primary_navigation',
        'menu_class' => 'site-navigation__menu',
        'container' => false,
        'depth' => 2,
      ]);
    endif;
    ?>

    <?php
      if (has_nav_menu('primary_navigation')) :
        wp_nav_menu([
          'theme_location' => 'primary_navigation',
          'menu_class' => 'site-navigation__menu site-navigation--secondary',
          'item_spacing' => 'discard',
          'container' => false,
          'sub_menu' => true,
          'depth' => 1,
        ]);
      endif;
    ?>
  </div>

  <button type="button" class="site-navigation__trigger"><span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>

</header>
