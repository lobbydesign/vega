<div class="flexbox-ie-fix">
  <div class="page-header page-header--home">
    <div class="page-header__fullscreen-bg">
      <video id="home-hero-video" playsinline loop muted autoplay class="page-header__fullscreen-video">
<?php /*        <source src="<?php echo get_template_directory_uri(); ?>/dist/images/vega-intro.mp4" type="video/mp4" media="(min-width:800px)">
        <source src="<?php echo get_template_directory_uri(); ?>/dist/images/vega-intro-mobile.mp4" type="video/mp4" media="(max-width:800px)"> */ ?>
      </video>
      <script>
        var video_container = document.getElementById('home-hero-video');

        if( window.innerWidth < 800 ) {
          video_container.innerHTML = '<source src="<?php echo get_template_directory_uri(); ?>/dist/images/vega-intro-mobile.mp4" type="video/mp4">';
        } else {
          video_container.innerHTML = '<source src="<?php echo get_template_directory_uri(); ?>/dist/images/vega-intro.mp4" type="video/mp4">';
        }
      </script>
    </div>
    <div class="page-header__content">
      <h1><img class="page-header__logo" src="<?php echo get_template_directory_uri(); ?>/dist/images/vega-logo-text.svg" width="475" height="229" alt="<?php bloginfo( 'name' ); ?> - <?php bloginfo('description'); ?>"></h1>
    </div>
    <div class="page-header__read-more">
      <button id="read-more" class="read-more"></button>
    </div>
    <div class="page-header__dim"></div>
  </div>
</div>