<div class="flexbox-ie-fix">
  <?php
    $image = get_field('heading_bakground_image', get_option('page_for_posts'));
    $image_arr = wp_get_attachment_image_src($image, 'page-header-bg');
  ?>
  <div class="page-header" style="background-image: url(<?php echo $image_arr[0]; ?>)">
  <div class="page-header__content">
    <h1 class="page-header__heading"><?php
    if( get_field('heading', get_option('page_for_posts')) ) { the_field('heading', get_option('page_for_posts')); } else { the_title(); } ?></h1>
  </div>
  <div class="page-header__read-more">
    <button id="read-more" class="read-more"></button>
  </div>
  <div class="page-header__dim"></div>
</div>
</div>