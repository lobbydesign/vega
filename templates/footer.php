<footer class="content-info">
  <div class="container">

    <img class="logo" src="<?php echo get_template_directory_uri(); ?>/dist/images/vega-logo.svg" width="150" height="56" alt="">

    <?php dynamic_sidebar('sidebar-footer'); ?>

    <div class="social">
      <a class="social__facebook" href="https://www.facebook.com/Vegastan-1210817662371320" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/dist/images/social-facebook.svg" width="35" height="35" alt="Följ oss på Facebook"></a>
      <a class="social__instagram" href="http://instagram.com/vegastan.se" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/dist/images/social-instagram.svg" width="35" height="35" alt="Följ oss på Instagram"></a>
      <a class="social__mail" href="mailto:anna@vegastan.se" title="Maila oss"><img src="<?php echo get_template_directory_uri(); ?>/dist/images/social-mail.svg" width="35" height="35" alt="Maila"></a>
    </div>

  </div>
</footer>
