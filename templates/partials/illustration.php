<div class="illustration">
  <?php
  $illustration_name = get_sub_field('illustration');
  $illustration_file = sprintf('%s/dist/images/illustration-%s.svg', get_template_directory(), $illustration_name );
  $illustration_file_small = sprintf('%s/dist/images/illustration-%s-small.svg', get_template_directory(), $illustration_name );
  ?>

  <div class="illustration__<?=$illustration_name; ?>">
    <?php

      if($illustration_name === 'vega') {

        if(file_exists( $illustration_file ))
          echo "<img class=\"illustration__vega__big\" src=\"" .get_template_directory_uri() . "/dist/images/illustration-$illustration_name.svg\">";

        if(file_exists( $illustration_file_small ))
          echo "<img class=\"illustration__vega__small\" src=\"" .get_template_directory_uri() . "/dist/images/illustration-$illustration_name-small.svg\">";

      } else {
        if(file_exists( $illustration_file ))
          include( $illustration_file );

        if(file_exists( $illustration_file_small ))
          include( $illustration_file_small );
      }

    ?>
  </div>

</div>


