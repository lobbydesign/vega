<div class="section">
  <div class="content">
    <div class="content__inner">

      <h2><?php the_sub_field('heading'); ?></h2>

      <?php
      // check if the repeater field has rows of data
      if( have_rows('questions') ): ?>
      <?php $i = 0; ?>
      <div id="accordion" role="tablist" aria-multiselectable="true">
        <?php  // loop through the rows of data
        while ( have_rows('questions') ) : the_row(); ?>

          <div class="card">
            <div class="card-header" role="tab" id="faq-heading-<?php echo $i; ?>">
              <a data-toggle="collapse" data-parent="#accordion" href="#faq-<?php echo $i; ?>" aria-expanded="<?php if($i === 0) { echo "true"; } else { echo "false"; } ?>" aria-controls="faq-<?php echo $i; ?>">
                <?php the_sub_field('question'); ?>
              </a>
            </div>

            <div id="faq-<?php echo $i; ?>" class="collapse <?php if($i === 0) { echo "show"; } ?>" role="tabpanel" aria-labelledby="faq-heading-<?php echo $i; ?>">
              <div class="card-block">
               <?php the_sub_field('answer'); ?>
              </div>
            </div>
          </div>
          <?php $i++; ?>
        <?php endwhile; ?>
      </div>
      <?php endif; ?>

    </div>
  </div>
</div>

