<?php
// Check if this section should use extra height
$classes = "";
if( get_sub_field('extra_height') ) {
  $classes .= ' section--extra-height';
}

// Should this section use a background image
$use_image = get_sub_field('use_image');
if( $use_image ) {
  $classes .= ' section--image_bg';
}

// Which side should the text be on
if(get_sub_field('text_adjustment')) {
  $classes .= ' section--align-' . get_sub_field('text_adjustment');
}

// Get the background color
$classes .= ' section--' . get_sub_field('primary_color');


?>

<div class="flexbox-ie-fix">
<div class="section <?php echo $classes; ?>">
  <div class="content">
    <div class="content__inner">
      <?php if( get_sub_field('heading') ) : ?>
        <h2><?php the_sub_field('heading'); ?></h2>
      <?php endif; ?>
      <?php the_sub_field('content'); ?>

      <?php if( get_sub_field('use_button') ): ?>
        <a class="btn" href="<?php the_sub_field('button_link'); ?>"><?php the_sub_field('button_text'); ?></a>
      <?php endif; ?>
    </div>
  </div>
  <?php if( $use_image ): ?>
  <div class="images">
    <?php $image = get_sub_field('image');
    $image_arr = wp_get_attachment_image_src($image, 'split-layout-image');
    ?>
    <div class="image" style="background-image:url(<?php echo $image_arr[0]; ?>);"></div>
    <?php
    $extra_image = get_sub_field('extra_image');
    $extra_image_arr = wp_get_attachment_image_src($extra_image, 'split-layout-image');
    if( $extra_image ) : ?>
      <div class="image" style="background-image:url(<?php echo $extra_image_arr[0]; ?>);"></div>
    <?php endif; ?>
  </div>
  <?php endif; ?>
</div>
</div>