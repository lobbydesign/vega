<div class="partial-image">

  <?php
  $image = get_sub_field('image');

  if( $image ) {
    echo wp_get_attachment_image( $image, 'full' );
  } ?>

</div>
