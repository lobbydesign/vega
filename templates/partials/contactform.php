<?php
// Check if this section should use extra height
$classes = "";

// Get the background color
$classes .= ' section--' . get_sub_field('primary_color');


?>

<div class="flexbox-ie-fix">
<div class="section section--form <?php echo $classes; ?>">
  <div class="content">
    <div class="content__inner">
      <?php if( get_sub_field('heading') ) : ?>
        <h2><?php the_sub_field('heading'); ?></h2>
      <?php endif; ?>
      <?php the_sub_field('content'); ?>

      <?php
      $contactform_id = get_sub_field('contactform');
      $contactform_shortcode = "[contact-form-7 id=\"$contactform_id\" title=\"\"]";
      $contactform_title = get_the_title( $contactform_id );

      echo do_shortcode( $contactform_shortcode );
      ?>

    </div>
  </div>
</div>
</div>

<script>
  jQuery(document).ready(function( $ ) {
    $('.wpcf7').on('wpcf7:mailsent', function(event){
      ga('send', 'event', 'Formulär', 'Skickad', '<?=$contactform_title;?>');
    });

    // Ajax triggers on form
    $('.wpcf7').on('submit',function(){
      $(this).addClass('wpcf7-sending-form');
      $(this).find('.btn').attr('disabled', 'disabled');
    });
    $(".wpcf7").on('wpcf7:submit wpcf7:spam wpcf7:mailsent wpcf7:mailfailed wpcf7:invalid', function(){
      $(this).removeClass('wpcf7-sending-form');
      $(this).find('.btn').removeAttr('disabled');
    });

  });
  </script>