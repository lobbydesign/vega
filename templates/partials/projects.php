<div class="projects-filters">
  <h4>Filtrera efter status</h4>

  <input type="radio" name="projects_filter" value="" checked id="filter_all">
  <label for="filter_all">Visa alla</label>

  <input type="radio" name="projects_filter" value="upcoming soon" id="filter_upcoming">
  <label for="filter_upcoming">Kommande</label>

  <input type="radio" name="projects_filter" value="ongoing" id="filter_ongoing">
  <label for="filter_ongoing">Pågående försäljning</label>

  <input type="radio" name="projects_filter" value="rented soldout" id="filter_soldout">
  <label for="filter_soldout">Slutsålt</label>
</div>

<div id="projects" class="projects">
  <?php
  $args = array(
    'post_type' => 'projects',
    'numberposts' => -1,
    'orderby' => 'rand'
  );
  $done_projects = "";
  $query = new WP_Query($args);
  if ( $query->have_posts() ) : while ( $query->have_posts() ) : $query->the_post(); ?>
    <?php
    $project_status = get_field('project_status');
    ob_start(); ?>
    <div class="flexbox-ie-fix project visible" data-project-status="<?= $project_status['value']; ?>">
      <div class="section section--image_bg">
        <div class="content">
          <?php
          if($project_status): ?>
            <div class="ribbon ribbon--<?php echo $project_status['value']; ?>">
              <div class="ribbon__inner"><?php echo $project_status['label']; ?></div>
            </div>
          <?php endif; ?>
          <div class="content__inner">
            <h2><?php the_title(); ?>, <?php the_field('project_developer'); ?></h2>
            <dl class="project__info">
              <?php if(get_field('project_scope')) : ?>
                <dt>Bostäder:</dt>
                <dd><?php the_field('project_scope'); ?></dd>
              <?php endif; ?>

              <?php if(get_field('project_occupancy')) : ?>
                <dt>Inflytt:</dt>
                <dd><?php the_field('project_occupancy'); ?></dd>
              <?php endif; ?>
            </dl>
            <?php the_content(); ?>

            <?php if( get_field('project_url') ): ?>
              <a target="_blank" class="btn" href="<?php the_field('project_url'); ?>" onclick="trackOutboundLink('<?php the_field('project_url'); ?>'); return false;">Till <?php the_title() ?></a>
            <?php endif; ?>
          </div>
        </div>
        <div class="images">
          <?php $image = get_field('project_image');
          $image_arr = wp_get_attachment_image_src($image, 'split-layout-image');
          ?>
          <div class="image" style="background-image:url(<?php echo $image_arr[0]; ?>);"></div>
        </div>
      </div>
    </div>
    <?php
    // Check if the project is done
    if( ($project_status['value'] == 'rented') || ($project_status['value'] == 'soldout') ) {
      // Add this project to the output after all other projects
      $done_projects .= ob_get_clean();
    } else {
      // Print project
      echo ob_get_clean();
    } ?>
  <?php endwhile;

  // Print all done projects
  echo $done_projects;

  endif;
  wp_reset_postdata(); ?>

  <div class="projects__error"><p>Tyvärr hittades inga bostäder med detta filter.</p></div>
</div>