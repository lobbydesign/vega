<script src='https://api.mapbox.com/mapbox-gl-js/v0.32.1/mapbox-gl.js'></script>
<link href='https://api.mapbox.com/mapbox-gl-js/v0.32.1/mapbox-gl.css' rel='stylesheet' />

<div id='map' class="map"></div>
<script>
mapboxgl.accessToken = 'pk.eyJ1IjoibWFyY3VzbG9iYnkiLCJhIjoiY2l6MnA1NXNyMDAzYzJxcGpsdjJrdHd0dyJ9.-wn0rVMMttfjR2ma4oaOSg';
var map = new mapboxgl.Map({
    container: 'map', // container id
    style: 'mapbox://styles/marcuslobby/ciz2ppota00342sphrkbze4dr', //stylesheet location
    center: [18.133,59.182], // starting position
    zoom: 13.1 // starting zoom
});

// disable map zoom when using scroll
map.scrollZoom.disable();

// Add zoom and rotation controls to the map.
map.addControl(new mapboxgl.NavigationControl());

</script>