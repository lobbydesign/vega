<?php

function cptui_register_my_cpts() {

  /**
   * Post Type: Projekt.
   */

  $labels = array(
    "name" => __( 'Projekt', 'sage' ),
    "singular_name" => __( 'Projekt', 'sage' ),
    "all_items" => __( 'Alla projekt', 'sage' ),
    "add_new" => __( 'Nytt projekt', 'sage' ),
  );

  $args = array(
    "label" => __( 'Projekt', 'sage' ),
    "labels" => $labels,
    "description" => "",
    "public" => false,
    "publicly_queryable" => false,
    "show_ui" => true,
    "show_in_rest" => true,
    "rest_base" => "",
    "has_archive" => false,
    "show_in_menu" => true,
    "exclude_from_search" => false,
    "capability_type" => "post",
    "map_meta_cap" => true,
    "hierarchical" => false,
    "rewrite" => array( "slug" => "projects", "with_front" => true ),
    "query_var" => true,
    "supports" => array( "title", "editor" ),
  );

  register_post_type( "projects", $args );
}

add_action( 'init', 'cptui_register_my_cpts' );
