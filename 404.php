<div class="page-header page-header--404">
  <div class="page-header__fullscreen-bg">
    <video loop muted autoplay class="page-header__fullscreen-video">
      <source src="<?php echo get_template_directory_uri(); ?>/dist/images/vega-intro.mp4" type="video/mp4">
      <source src="<?php echo get_template_directory_uri(); ?>/dist/images/vega-intro.webm" type="video/webm">
      <source src="<?php echo get_template_directory_uri(); ?>/dist/images/vega-intro.ogv" type="video/ogg">
    </video>
  </div>
  <div class="page-header__content">
    <h1>Sidan kunde inte hittas</h1>
      <p>Vi ber om ursäkt, men på den här adressen verkar det inte finnas någon sida.</p>
      <a href="#" class="btn btn-outline-white">Gå till startsidan</a>
  </div>
  <div class="page-header__dim"></div>
</div>