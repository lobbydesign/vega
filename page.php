<?php
use Roots\Sage\ACF_Layout;
?>

<?php while (have_posts()) : the_post(); ?>
  <?php if( is_front_page() ) {
    get_template_part('templates/page', 'header-home');
  } else {
    get_template_part('templates/page', 'header');
  } ?>
  <?php get_template_part('templates/content', 'page'); ?>


  <?php if( have_rows('flexible_content') ): ?>
    <div id="" class="flexible_layout">
      <?php while (have_rows('flexible_content')) : the_row() ?>
        <?php ACF_Layout\ACF_Layout::render( get_row_layout() ); ?>
      <?php endwhile; ?>
    </div>
  <?php endif; ?>


<?php endwhile; ?>
