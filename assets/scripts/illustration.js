(function($) {

  var sun = $('#sol');

  new TweenMax.from('#Sol', 5, {
    rotation: -360,
    transformOrigin: "center",
    ease: "linear",
    repeat: -1
  });

  // Molnen
  new TweenMax.to('#Moln_1', 3,   {yPercent: -50, repeat: -1, yoyo: true, ease: Power1.easeInOut });
  new TweenMax.to('#Moln_2', 2,   {yPercent: -45, repeat: -1, yoyo: true, ease: Power1.easeInOut });
  new TweenMax.to('#Moln_3', 3.3, {yPercent: -55,  repeat: -1, yoyo: true, ease: Power1.easeInOut });
  new TweenMax.to('#Moln_4', 4,   {yPercent: -44,  repeat: -1, yoyo: true, ease: Power1.easeInOut });
  new TweenMax.to('#Moln_5', 2,   {yPercent: -48,  repeat: -1, yoyo: true, ease: Power1.easeInOut });
  new TweenMax.to('#Moln_6', 2.5,   {yPercent: -48,  repeat: -1, yoyo: true, ease: Power1.easeInOut });

  new TweenMax.to('#Hållbarhet', 1.5, {
    rotation: 360,
    transformOrigin: "center",
    ease: "linear",
    repeat: -1
  });

  new TweenMax.from('#Pendeltåg', 10, {
    ease: Power2.easeOut,
    xPercent: 300,
    yoyo: true,
    repeat: -1,
    repeatDelay: 3
  });

  new TweenMax.to('#Cykel', 2, {
    xPercent: 120,
    yoyo: true,
    repeat: -1,
    repeatDelay: 3,
    ease: Power3.easeInOut,
  });

  // Drönaren
  var drone = new TimelineMax({repeat:-1}),
    droneObj = $('#Drönare');
  drone
    .to( droneObj, 0,   {rotation: 20, transformOrigin: 'center', xPercent: 50})
    .to( droneObj, 0.5,   {rotation: -20, transformOrigin: 'center'})
    .to( droneObj, 1.2, {xPercent: -50, ease: Power1.easeOut})
    .to( droneObj, 0.5,   {rotation: 20, transformOrigin: 'center'})
    .to( droneObj, 1.2, {xPercent: 50,  ease: Power1.easeOut});



  TweenMax.to('#Luftballong', 5, {
    bezier:[
      {x:0, y:0},
      {x:20, y:8},
      {x:50, y:-10},
      {x:68, y:-13},
      {x:100, y:0}
    ],
    ease:Power1.easeInOut,
    repeat: -1,
    yoyo: true,
    transformOrigin: '50% 15%',
    repeatDelay: 0.5,
  });

  $('#Luftballong').on('click', function(){
    TweenMax.to('#Luftballong', 2, {
      scale: 0,
      x: 0,
      y: 0,
      ease:Power1.easeInOut,
      opacity: 0
    });
  });


  // Båten
  new TweenMax.to('#Båtflagga', 0.5, {y: -5, yoyo: true, repeat: -1});
  var boat = new TimelineMax({repeat:-1}),
      boatObj = $('#Båt');

  boat
    .to( boatObj, 0,   {rotation: -10, transformOrigin: '50% 10%', xPercent: 10})
    .to( boatObj, 3, {rotation: 10, transformOrigin: '50% 10%', xPercent: -10, ease: Power1.easeInOut, delay: 0.5})
    .to( boatObj, 3, {rotation: -10, transformOrigin: '50% 10%', xPercent: 10, ease: Power1.easeInOut, delay: 0.5});

})(jQuery);