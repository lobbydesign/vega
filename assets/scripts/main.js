/* ========================================================================
 * DOM-based Routing
 * Based on http://goo.gl/EUTi53 by Paul Irish
 *
 * Only fires on body classes that match. If a body class contains a dash,
 * replace the dash with an underscore when adding it to the object below.
 *
 * .noConflict()
 * The routing is enclosed within an anonymous function so that you can
 * always reference jQuery with $, even when in .noConflict() mode.
 * ======================================================================== */

(function($) {

  var project_filters = $('input:radio[name=projects_filter]');
  var $projects = $('.projects .project');

  project_filters.on('change', function() {
    var $checked = project_filters.filter(':checked');
    var filter_value = $checked.val();

    $projects.addClass('visible filtered').removeClass('odd even').show();

    $projects.filter(function(){
      var attr = $(this).data('project-status');

      var filterArr = filter_value.split(" ");

      if( filter_value === '' ) {
        return false;
      }

      return $.inArray( attr, filterArr) === -1;
    }).hide().removeClass('visible');

    $('.projects .project:visible').each(function(index) {
      if (index %2 === 1) {
        $(this).addClass('even');
      } else {
        $(this).addClass('odd');
      }
    });

    // Check if we got any visible divs
    if( $('.projects .project:visible').length === 0 ) {
      $('.projects__error').show();
    } else {
      $('.projects__error').hide();
    }

  });

  //
  $('.site-navigation__trigger').on('click', function(e) {
    e.preventDefault();
    $('html').toggleClass('state-sitenav-is-open');
  });

  // Shuffle projects
  // var projects = $("#projects");
  // var divs = projects.children();
  // while (divs.length) {
  //     projects.append(divs.splice(Math.floor(Math.random() * divs.length), 1)[0]);
  // }

  // Check if we have scrolled
  var header = $(".site-navigation"),
      headerScrollPosition = header.offset().top + header.outerHeight(),
      headerAnimating = false,
      lastScrollTop = 0;

  $(window).scroll(function() {
    var scroll = $(window).scrollTop();

    lastScrollTop = scroll;

    if (scroll > 0) {
      header.addClass("scrolled");
    } else {
      header.removeClass("scrolled");
    }
  });


  // Read more
  $("#read-more").click(function() {
    $('html, body').animate({
      scrollTop: $(".flexible_layout").offset().top - 74
    }, 500);
  });

  // Use this variable to set up the common and page specific functions. If you
  // rename this variable, you will also need to rename the namespace below.
  var Sage = {
    // All pages
    'common': {
      init: function() {
        // JavaScript to be fired on all pages
      },
      finalize: function() {
        // JavaScript to be fired on all pages, after page specific JS is fired
      }
    },
    // Home page
    'home': {
      init: function() {
        // JavaScript to be fired on the home page
      },
      finalize: function() {
        // JavaScript to be fired on the home page, after the init JS
      }
    },
    // About us page, note the change from about-us to about_us.
    'about_us': {
      init: function() {
        // JavaScript to be fired on the about us page
      }
    }
  };

  // The routing fires all common scripts, followed by the page specific scripts.
  // Add additional events for more control over timing e.g. a finalize event
  var UTIL = {
    fire: function(func, funcname, args) {
      var fire;
      var namespace = Sage;
      funcname = (funcname === undefined) ? 'init' : funcname;
      fire = func !== '';
      fire = fire && namespace[func];
      fire = fire && typeof namespace[func][funcname] === 'function';

      if (fire) {
        namespace[func][funcname](args);
      }
    },
    loadEvents: function() {
      // Fire common init JS
      UTIL.fire('common');

      // Fire page-specific init JS, and then finalize JS
      $.each(document.body.className.replace(/-/g, '_').split(/\s+/), function(i, classnm) {
        UTIL.fire(classnm);
        UTIL.fire(classnm, 'finalize');
      });

      // Fire common finalize JS
      UTIL.fire('common', 'finalize');
    }
  };

  // Load Events
  $(document).ready(UTIL.loadEvents);

})(jQuery); // Fully reference jQuery after this point.
