<?php get_template_part('templates/page', 'header-blog'); ?>

<div class="flexible_layout posts-archive">

<?php if (!have_posts()) : ?>
  <div class="flexbox-ie-fix">
  <div class="section">
    <div class="content">
      <div class="content__inner">
        <div class="alert alert-warning">
          <?php _e('Sorry, no results were found.', 'sage'); ?>
        </div>
      </div>
    </div>
  </div>
  </div>
<?php endif; ?>

<?php while (have_posts()) : the_post(); ?>

  <div class="flexbox-ie-fix">
  <div class="section">
    <div class="content">
      <div class="content__inner">

  <?php get_template_part('templates/content', get_post_type() != 'post' ? get_post_type() : get_post_format()); ?>

      </div>
    </div>
  </div>
  </div>

<?php endwhile; ?>

<?php the_posts_navigation(); ?>

</div>